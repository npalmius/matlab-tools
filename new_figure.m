function [h_out] = new_figure(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 06-Sep-2016

    args = varargin;
    
    h = NaN;
    
    if length(varargin) >= 1
        if isa(varargin{1}, 'matlab.ui.Figure') || ((numel(varargin{1}) == 1) && isnan(varargin{1}))
            h = varargin{1};
            if length(varargin) > 1
                args = varargin(2:end);
            else
                args = {};
            end
        end
    end
    
    new_figure = get_argument({'new_figures', 'newfigures', 'new_figure', 'newfigure'}, true, args{:});
    create_figure = get_argument({'create_figures', 'createfigures', 'create_figure', 'createfigure'}, true, args{:});
    create_new_figure = get_argument({'create_new_figures', 'createnewfigures', 'create_new_figure', 'createnewfigure'}, true, args{:});
    show_figure = get_argument({'show_figures', 'showfigures', 'show_figure', 'showfigure'}, true, args{:});
    
    if new_figure && create_figure && create_new_figure && show_figure
        if isa(h, 'matlab.ui.Figure')
            h_out_temp = figure(h);
        else
            h_out_temp = figure;
        end
    else
        h_out_temp = NaN;
    end
    
    if nargout > 0
        h_out = h_out_temp;
    end
end
