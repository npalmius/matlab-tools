# MATLAB Tools

This repository contains a selection of MATLAB tools.

---

[![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) except as detailed in individual source files.