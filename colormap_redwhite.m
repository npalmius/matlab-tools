function c = colormap_redwhite(m, max_brightness)
%COLORMAP_REDWHITE    Shades of red to white color map
%   COLORMAP_REDWHITE(M), is an M-by-3 matrix that defines a colormap.
%   The colors begin with white and ranges through shades of
%   red to bright red. COLORMAP_REDWHITE, by itself, is the same
%   length as the current figure's colormap. If no figure exists,
%   MATLAB creates one.
%
%   COLORMAP_REDWHITE(..., MAX_BRIGHTNESS) specifies the maximum
%   brightness of the red end of the colormap. Must be
%   greater then 0 and less than or equal to 1, default is 1,
%   corresponding to full brightness.
%
%   For example, to reset the colormap of the current figure:
%
%             colormap(colormap_redwhite)
%
%   See also HSV, GRAY, HOT, BONE, COPPER, PINK, FLAG, 
%   COLORMAP, RGBPLOT, COLORMAP_REDBLUE.

% Original version Copyright (c) 2009, Adam Auton
% All rights reserved.
%
% Modified version Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally by Adam Auton, 9th October 2009
% Modified by Nick Palmius, 05-Oct-2015

if nargin < 1, m = size(get(gcf,'colormap'),1); end
if nargin < 2, max_brightness = 1; end

% From [1 1 1] to [1 0 0];
g = flipud((((0:m-1)'/max(m-1,1)) * max_brightness) + (1 - max_brightness));
r = ones(m,1);
b = g;

c = [r g b]; 

