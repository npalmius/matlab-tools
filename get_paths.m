function paths = get_paths(varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    this_path = fileparts(mfilename('fullpath'));
    
    this_path = get_argument({'rootpath', 'root_path'}, this_path, varargin{:});
    
    if (~isempty(this_path)) && (this_path(end) ~= '/') && (this_path(end) ~= '\')
        this_path = [this_path '/'];
    end
    
    paths = struct();
    
    paths.script_path = this_path;
    
    paths.working_path = get_argument({'working_path', 'workingpath', 'working_dir', 'workingdir'}, [this_path 'working/'], varargin{:});
    paths.output_path = get_argument({'output_path', 'outputpath', 'output_dir', 'outputdir'}, [paths.working_path 'output/'], varargin{:});
    
    paths.prefer_working = get_argument({'preferworking', 'prefer_working'}, true, varargin{:});
    paths.output_sub_dir = get_argument({'outputsubdir', 'output_subdir', 'output_sub_dir'}, '', varargin{:});
    paths.working_sub_dir = get_argument({'workingsubdir', 'working_subdir', 'working_sub_dir'}, '', varargin{:});
    
    if (~isempty(paths.output_sub_dir)) && (paths.output_sub_dir(end) ~= '/') && (paths.output_sub_dir(end) ~= '\')
        paths.output_sub_dir = [paths.output_sub_dir '/'];
    end

    paths.output_path = [paths.output_path paths.output_sub_dir];

    if (~isempty(paths.working_sub_dir)) && (paths.working_sub_dir(end) ~= '/') && (paths.working_sub_dir(end) ~= '\')
        paths.working_sub_dir = [paths.working_sub_dir '/'];
    end

    paths.working_path = [paths.working_path paths.working_sub_dir];
end
