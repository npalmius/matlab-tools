function [] = init_module(module_name, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    matlab_tools_path = fileparts(mfilename('fullpath'));
    parent_path = fileparts(matlab_tools_path);
    
    if (~isempty(parent_path)) && (parent_path(end) ~= '/') && (parent_path(end) ~= '\')
        parent_path = [parent_path '/'];
    end
    
    module_root_paths = {parent_path};
    
    if exist([matlab_tools_path '/module_search_paths.lst'], 'file') == 2
        f = fopen([matlab_tools_path '/module_search_paths.lst']);
        line = fgetl(f);
        while ischar(line)
            k = strfind(line, '#');
            if ~isempty(k)
                line = line(1:(k(1)-1));
            end
            line = strtrim(line);
            if ~isempty(line)
                if (line(end) ~= '/') && (line(end) ~= '\')
                    line = [line '/']; %#ok<AGROW>
                end
                if exist(line, 'dir')
                    module_root_paths = [module_root_paths, line]; %#ok<AGROW>
                else
                    fprintf('Warning: Path ''%s'' in module_roots.lst does not exist\n', line)
                end
            end
            line = fgetl(f);
        end
        fclose(f);
    end
    
    module_paths = cell(length(module_root_paths) * 2, 1);
    
    for i = 1:length(module_root_paths)
        module_paths{(i - 1) * 2 + 1} = [module_root_paths{i} module_name];
        module_paths{(i - 1) * 2 + 2} = [module_root_paths{i} 'MATLAB-' module_name];
    end
    
    module_path = [];
    
    for i = 1:numel(module_paths)
        if exist(module_paths{i}, 'dir') == 7
            module_path = module_paths{i};
            if exist([module_path '/Init/init.m'], 'file') == 2
                break;
            end
        end
    end
    
    assert(exist(module_path, 'dir') == 7, 'Module %s not found.', module_name);
    
    module_init_path = [module_path '/Init/init.m'];
    
    assert(exist(module_init_path, 'file') == 2, 'Module %s cannot be initialised.', module_name);
    
    run(module_init_path);
end
