function [out] = half_sample_mode(x_raw)
% Copyright (c) 2017, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 24-Apr-2017

% References:
% * Robertson, T., & Cryer, J. D. (1974). An Iterative Procedure for
%   Estimating the Mode. Journal of the American Statistical Association,
%   69(348), 1012�1016.
%   Retrieved from http://doi.org/10.1080/01621459.1974.10480246
% * Bickel, D. R., & Fr�hwirth, R. (2006). On a fast, robust estimator of
%   the mode: Comparisons to other robust estimators with applications.
%   Computational Statistics & Data Analysis, 50(12), 3500�3530.
%   Retrieved from http://doi.org/http://doi.org/10.1016/j.csda.2005.07.011

    x = sort(x_raw);
    
    if numel(x) == 1
        out = x;
    elseif numel(x) == 2
        out = mean(x);
    elseif numel(x) == 3
        if x(2) - x(1) < x(3) - x(2)
            out = mean(x(1:2));
        else
            out = mean(x(2:3));
        end
    else
        w_min = x(end) - x(1);
        
        N = ceil(numel(x) / 2);
        
        j = 1;
        
        for i = 1:(numel(x) - N + 1)
            w = x(i + N - 1) - x(i);
            if w < w_min
                w_min = w;
                j = i;
            end
        end
        
        out = half_sample_mode(x(j:(j + N - 1)));
    end
end
