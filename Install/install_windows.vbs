Option Explicit

Dim wsh, fso

Set wsh = CreateObject("WScript.Shell")
Set fso = CreateObject("Scripting.FileSystemObject")

Dim strInitFilePath, strInputFile, strOutputFile

strOutputFile = wsh.SpecialFolders("MyDocuments")
If right(strOutputFile, 1) <> "\" Then
  strOutputFile = strOutputFile & "\"
End If
strOutputFile = strOutputFile & "MATLAB"

If Not fso.FolderExists(strOutputFile) then
  fso.CreateFolder(strOutputFile)
End If

strOutputFile = strOutputFile & "\startup.m"

strInitFilePath = fso.GetParentFolderName(fso.GetParentFolderName(Wscript.ScriptFullName))
If right(strInitFilePath, 1) <> "\" Then
  strInitFilePath = strInitFilePath & "\"
End If
strInitFilePath = strInitFilePath & "Init\init.m"

If Not fso.FileExists(strOutputFile) Then
  Dim txtIn, txtOut, strFileContent
  
  strInputFile = fso.GetParentFolderName(Wscript.ScriptFullName)
  If right(strInputFile, 1) <> "\" Then
    strInputFile = strInputFile & "\"
  End If
  strInputFile = strInputFile & "startup-template.m"

  Set txtIn = fso.OpenTextFile(strInputFile, 1, False)
  Set txtOut = fso.OpenTextFile(strOutputFile, 2, True)
  
  strFileContent = txtIn.ReadAll
  strFileContent = Replace(strFileContent, "<INIT_PATH>", strInitFilePath)
  
  Call txtOut.Write(strFileContent)
  
  Call txtIn.Close
  Call txtOut.Close
  
  Call wscript.echo("Matlab startup script created at " & strInputFile & ".")
Else
  Dim strMessage
  
  strMessage = "Matlab startup script already exists at " & strOutputFile & "." & vbCrLf & vbCrLf
  strMessage = strMessage & "Add the following line the startup script to install MATLAB-Tools:" & vbCrLf & vbCrLf
  strMessage = strMessage & "run('" & strInitFilePath & "');" & vbCrLf
  
  Call wscript.echo(strMessage)
End If
